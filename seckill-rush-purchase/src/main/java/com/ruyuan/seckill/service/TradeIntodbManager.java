package com.ruyuan.seckill.service;

import com.ruyuan.seckill.domain.vo.TradeVO;

/**
 * 交易入库业务接口
 */
public interface TradeIntodbManager {

    /**
     * redis库存扣减
     *
     * @param tradeVO 订单信息
     */
    void seckillStockDeduction(TradeVO tradeVO);
}
