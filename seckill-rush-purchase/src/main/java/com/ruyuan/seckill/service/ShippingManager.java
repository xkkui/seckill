package com.ruyuan.seckill.service;


import com.ruyuan.seckill.domain.vo.CacheGoods;
import com.ruyuan.seckill.domain.vo.CartVO;

import java.util.List;
import java.util.Map;

/**
 * 运费计算业务层接口
 */
public interface ShippingManager {

    /**
     * 获取各个商家的运费
     *
     * @param cartList 购物车
     * @param areaId   地区
     * @return
     */
    Map<Integer, Double> getShippingPrice(List<CartVO> cartList, Integer areaId);

    /**
     * 设置运费
     *
     * @param cartList 购物车集合
     */
    void setShippingPrice(List<CartVO> cartList,Integer uid);

    /**
     * 设置运费
     *
     * @param cartList 购物车集合
     */
    void setShippingPrice(List<CartVO> cartList);
    /**
     * 检测是否有不能配送的区域
     *
     * @param cartList 购物车
     * @param areaId   地区
     * @return
     */
    List<CacheGoods> checkArea(List<CartVO> cartList, Integer areaId);
}
