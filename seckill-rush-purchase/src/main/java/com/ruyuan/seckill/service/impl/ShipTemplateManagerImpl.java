package com.ruyuan.seckill.service.impl;


import com.ruyuan.seckill.cache.Cache;
import com.ruyuan.seckill.domain.enums.CachePrefix;
import com.ruyuan.seckill.domain.vo.ShipTemplateVO;
import com.ruyuan.seckill.service.GoodsClient;
import com.ruyuan.seckill.service.ShipTemplateManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 运费模版业务类
 */
@Service
public class ShipTemplateManagerImpl implements ShipTemplateManager {



    @Autowired
    private Cache cache;

    @Autowired
    private GoodsClient goodsClient;

    // @Autowired
    // private RegionsClient regionsClient;
    //
    // @Autowired
    // private AmqpTemplate amqpTemplate;
    //
    // private final Logger logger = LoggerFactory.getLogger(getClass());
    //
    //
    // @Override
    // @Transactional(value = "memberTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    // public ShipTemplateDO save(ShipTemplateSellerVO template) {
    //     template.setSellerId(UserContext.getSeller().getSellerId());
    //     ShipTemplateDO t = new ShipTemplateDO();
    //     BeanUtils.copyProperties(template, t);
    //
    //     this.daoSupport.insert(t);
    //     int id = this.daoSupport.getLastId("es_ship_template");
    //     t.setId(id);
    //     //保存运费模板子模板
    //     List<ShipTemplateChildSellerVO> items = template.getItems();
    //     List<ShipTemplateChildSellerVO> freeItems = template.getFreeItems();
    //
    //     //设置指定运费子模板
    //     this.addTemplateChildren(items, id,false);
    //     //设置指定免运费子模板
    //     this.addTemplateChildren(freeItems, id,true);
    //
    //
    //     cache.remove(CachePrefix.SHIP_TEMPLATE.getPrefix() + template.getSellerId());
    //
    //     this.amqpTemplate.convertAndSend(AmqpExchange.SHIP_TEMPLATE_CHANGE, AmqpExchange.SHIP_TEMPLATE_CHANGE + "_ROUTING", new ShipTemplateMsg(id, 1));
    //     return t;
    // }
    //
    // @Override
    // @Transactional(value = "memberTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    // public ShipTemplateDO edit(ShipTemplateSellerVO template) {
    //     template.setSellerId(UserContext.getSeller().getSellerId());
    //     ShipTemplateDO t = new ShipTemplateDO();
    //     BeanUtils.copyProperties(template, t);
    //
    //     Integer id = template.getId();
    //     this.daoSupport.update(t, id);
    //     //删除子模板
    //     this.daoSupport.execute("delete from es_ship_template_child where template_id = ?", id);
    //
    //     //保存运费模板子模板
    //     List<ShipTemplateChildSellerVO> items = template.getItems();
    //     List<ShipTemplateChildSellerVO> freeItems = template.getFreeItems();
    //
    //     this.addTemplateChildren(items, id,false);
    //
    //     this.addTemplateChildren(freeItems, id,true);
    //
    //     //移除缓存某个VO
    //     this.cache.remove(CachePrefix.SHIP_TEMPLATE_ONE.getPrefix() + id);
    //
    //     this.cache.remove(CachePrefix.SHIP_TEMPLATE.getPrefix() + template.getSellerId());
    //
    //     this.amqpTemplate.convertAndSend(AmqpExchange.SHIP_TEMPLATE_CHANGE, AmqpExchange.SHIP_TEMPLATE_CHANGE + "_ROUTING", new ShipTemplateMsg(id, 2));
    //
    //
    //     return t;
    // }
    //
    // /**
    //  * 添加运费模板子模板
    //  */
    // private void addTemplateChildren(List<ShipTemplateChildSellerVO> items, Integer templateId,boolean isFree) {
    //
    //     for (ShipTemplateChildSellerVO child : items) {
    //
    //         ShipTemplateChild shipTemplateChild = new ShipTemplateChild();
    //         BeanUtils.copyProperties(child, shipTemplateChild);
    //         shipTemplateChild.setTemplateId(templateId);
    //         //获取地区id
    //         String area = child.getArea();
    //
    //         Gson gson = new Gson();
    //         Map<String, Map> map = new HashMap();
    //         map = gson.fromJson(area, map.getClass());
    //         StringBuffer areaIdBuffer = new StringBuffer(",");
    //         // 获取所有的地区
    //         Object obj = this.cache.get(CachePrefix.REGIONALL.getPrefix() + 4);
    //         List<RegionVO> allRegions = null;
    //         if (obj == null) {
    //             obj = regionsClient.getRegionByDepth(4);
    //         }
    //         allRegions = (List<RegionVO>) obj;
    //         Map<String, RegionVO> regionsMap = new HashMap();
    //         //循环地区放到Map中，便于取出
    //         for (RegionVO region : allRegions) {
    //             regionsMap.put(region.getId() + "", region);
    //         }
    //
    //         for (String key : map.keySet()) {
    //             //拼接地区id
    //             areaIdBuffer.append(key + ",");
    //             Map dto = map.get(key);
    //             //需要取出改地区下面所有的子地区
    //             RegionVO provinceRegion = regionsMap.get(key);
    //             List<RegionVO> cityRegionList = provinceRegion.getChildren();
    //
    //             Map<String, RegionVO> cityRegionMap = new HashMap<>();
    //             for (RegionVO city : cityRegionList) {
    //                 cityRegionMap.put(city.getId() + "", city);
    //             }
    //             //判断下面的地区是否被全选
    //             if ((boolean) dto.get("selected_all")) {
    //
    //                 //市
    //                 for (RegionVO cityRegion : cityRegionList) {
    //
    //                     areaIdBuffer.append(cityRegion.getId() + ",");
    //                     List<RegionVO> regionList = cityRegion.getChildren();
    //                     //区
    //                     for (RegionVO region : regionList) {
    //
    //                         areaIdBuffer.append(region.getId() + ",");
    //                         List<RegionVO> townList = region.getChildren();
    //                         //城镇
    //                         if (townList != null) {
    //                             for (RegionVO townRegion : townList) {
    //
    //                                 areaIdBuffer.append(townRegion.getId() + ",");
    //                             }
    //                         }
    //                     }
    //                 }
    //             } else {
    //                 //没有全选，则看选中城市
    //                 Map<String, Map> citiesMap = (Map<String, Map>) dto.get("children");
    //                 for (String cityKey : citiesMap.keySet()) {
    //
    //                     areaIdBuffer.append(cityKey + ",");
    //
    //                     Map cityMap = citiesMap.get(cityKey);
    //
    //                     RegionVO cityRegion = cityRegionMap.get(cityKey);
    //                     List<RegionVO> regionList = cityRegion.getChildren();
    //                     //某个城市如果全部选中，需要取出城市下面的子地区
    //                     if ((boolean) cityMap.get("selected_all")) {
    //                         //区
    //                         for (RegionVO region : regionList) {
    //
    //                             areaIdBuffer.append(region.getId() + ",");
    //                             List<RegionVO> townList = region.getChildren();
    //                             //城镇
    //                             if (townList != null) {
    //                                 for (RegionVO townRegion : townList) {
    //
    //                                     areaIdBuffer.append(townRegion.getId() + ",");
    //                                 }
    //                             }
    //                         }
    //
    //                     } else {
    //                         //选中了某个城市下面的几个区
    //                         Map<String, Map> regionMap = (Map<String, Map>) cityMap.get("children");
    //                         for (String regionKey : regionMap.keySet()) {
    //
    //                             areaIdBuffer.append(regionKey + ",");
    //                             for (RegionVO region : regionList) {
    //                                 if (("" + region.getId()).equals(regionKey)) {
    //                                     List<RegionVO> townList = region.getChildren();
    //                                     //城镇
    //                                     if (townList != null) {
    //                                         for (RegionVO townRegion : townList) {
    //
    //                                             areaIdBuffer.append(townRegion.getId() + ",");
    //                                         }
    //                                     }
    //                                     break;
    //                                 }
    //                             }
    //                         }
    //                     }
    //                 }
    //             }
    //         }
    //         shipTemplateChild.setAreaId(areaIdBuffer.toString());
    //         shipTemplateChild.setIsFree(isFree);
    //         this.daoSupport.insert(shipTemplateChild);
    //     }
    //
    // }
    //
    // @Override
    // public List<ShipTemplateSellerVO> getStoreTemplate(Integer sellerId) {
    //     cache.remove(CachePrefix.SHIP_TEMPLATE.getPrefix() + sellerId);
    //     List<ShipTemplateSellerVO> list = (List<ShipTemplateSellerVO>) cache.get(CachePrefix.SHIP_TEMPLATE.getPrefix() + sellerId);
    //     if (list == null) {
    //         list = this.daoSupport.queryForList("select * from es_ship_template where seller_id = ? ", ShipTemplateSellerVO.class,
    //                 sellerId);
    //
    //         if (list != null) {
    //             for (ShipTemplateSellerVO vo : list) {
    //                 String sql = "select first_company,first_price,continued_company,continued_price,area from es_ship_template_child where template_id = ?";
    //                 List<ShipTemplateChild> children = this.daoSupport.queryForList(sql, ShipTemplateChild.class, vo.getId());
    //                 List<ShipTemplateChildSellerVO> items = new ArrayList<>();
    //                 if (children != null) {
    //                     for (ShipTemplateChild child : children) {
    //                         ShipTemplateChildSellerVO childvo = new ShipTemplateChildSellerVO(child, true);
    //                         items.add(childvo);
    //                     }
    //                 }
    //                 vo.setItems(items);
    //             }
    //         }
    //         cache.put(CachePrefix.SHIP_TEMPLATE.getPrefix() + sellerId, list);
    //     }
    //
    //     return list;
    // }
    //
    // @Override
    // @Transactional(value = "memberTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    // public void delete(Integer templateId) {
    //     GoodsDO goodsDO = this.goodsClient.checkShipTemplate(templateId);
    //     if (goodsDO != null) {
    //         throw new ServiceException(ShopErrorCode.E226.code(), "模版被商品【" + goodsDO.getGoodsName() + "】使用，无法删除该模版");
    //     }
    //
    //     ShipTemplateDO template = this.getOneDB(templateId);
    //
    //     //删除运费模板
    //     this.daoSupport.execute("delete from es_ship_template where id=?", templateId);
    //     //删除运费模板关联地区
    //     this.daoSupport.execute("delete from es_ship_template_child where template_id = ?", templateId);
    //
    //     Integer sellerId = template.getSellerId();
    //
    //     //移除缓存某个VO
    //     this.cache.remove(CachePrefix.SHIP_TEMPLATE_ONE.getPrefix() + templateId);
    //     //移除缓存某商家的VO列表
    //     this.cache.remove(CachePrefix.SHIP_TEMPLATE.getPrefix() + sellerId);
    // }

    @Override
    public ShipTemplateVO getFromCache(Integer templateId) {
        ShipTemplateVO tpl = (ShipTemplateVO) this.cache.get(CachePrefix.SHIP_TEMPLATE_ONE.getPrefix() + templateId);

        return tpl;

    }
    //
    // @Override
    // public ShipTemplateSellerVO getFromDB(Integer templateId) {
    //
    //     ShipTemplateDO template = this.getOneDB(templateId);
    //     ShipTemplateSellerVO tpl = new ShipTemplateSellerVO();
    //     BeanUtils.copyProperties(template, tpl);
    //
    //     //查询运费模板的子模板
    //     String sql = "select first_company,first_price,continued_company,continued_price,area from es_ship_template_child where template_id = ?";
    //     List<ShipTemplateChild> children = this.daoSupport.queryForList(sql, ShipTemplateChild.class, templateId);
    //
    //     List<ShipTemplateChildSellerVO> items = new ArrayList<>();
    //     if (children != null) {
    //         for (ShipTemplateChild child : children) {
    //             ShipTemplateChildSellerVO childvo = new ShipTemplateChildSellerVO(child, false);
    //             items.add(childvo);
    //         }
    //     }
    //
    //     tpl.setItems(items);
    //
    //     return tpl;
    // }
    //
    // @Override
    // public List<String> getScripts(Integer id) {
    //
    //     List<String> scripts = (List<String>) cache.get(CachePrefix.SHIP_SCRIPT.getPrefix() + id);
    //     if (scripts == null) {
    //         ShipTemplateVO shipTemplateVO = this.getFromCache(id);
    //         return this.cacheShipTemplateScript(shipTemplateVO);
    //     }
    //     return scripts;
    // }
    //
    // @Override
    // public List<String> cacheShipTemplateScript(ShipTemplateVO shipTemplateVO) {
    //     //获取运费模板子模板
    //     List<ShipTemplateChildBuyerVO> items = shipTemplateVO.getItems();
    //
    //     //获取免运费子模板
    //     List<ShipTemplateChildBuyerVO> freeItems = shipTemplateVO.getFreeItems();
    //
    //     //免运费区域areaids
    //     StringBuffer freeAreaIds = new StringBuffer();
    //
    //     for (ShipTemplateChildBuyerVO buyerVO:freeItems) {
    //         freeAreaIds.append(buyerVO.getAreaId());
    //     }
    //
    //     List<String> scripts = new ArrayList<>();
    //     //循环子模板生成script
    //     for (ShipTemplateChildBuyerVO shipTemplateChildBuyerVO : items) {
    //         String script = getScript(shipTemplateChildBuyerVO, shipTemplateVO.getType(),freeAreaIds.toString());
    //         if (StringUtil.isEmpty(script)) {
    //             logger.error("运费模板id为" + shipTemplateVO.getId() + "的模板生成错误");
    //         }
    //         scripts.add(script);
    //     }
    //
    //     //缓存模板script，格式为SHIP_SCRIPT_模板id
    //     cache.put(CachePrefix.SHIP_SCRIPT.getPrefix() + shipTemplateVO.getId(), scripts);
    //
    //     return scripts;
    // }
    //
    // /**
    //  * 生成脚本
    //  *
    //  * @param shipTemplateChildBuyerVO 子模板信息
    //  * @param shipTempalteType         模板类型
    //  * @return script脚本
    //  */
    // private String getScript(ShipTemplateChildBuyerVO shipTemplateChildBuyerVO, Integer shipTempalteType,String freeAreaIds) {
    //     Map<String, Object> params = new HashMap<>();
    //     params.put("area", shipTemplateChildBuyerVO.getAreaId());
    //     params.put("firstPrice", shipTemplateChildBuyerVO.getFirstPrice());
    //     params.put("firstCompany", shipTemplateChildBuyerVO.getFirstCompany());
    //     params.put("continuedCompany", shipTemplateChildBuyerVO.getContinuedCompany());
    //     params.put("continuedPrice", shipTemplateChildBuyerVO.getContinuedPrice());
    //     params.put("freeAreaIds", freeAreaIds);
    //     params.put("type", shipTempalteType);
    //     String path = "ship.ftl";
    //     String script = ScriptUtil.renderScript(path, params);
    //     logger.debug("生成运费脚本：" + script);
    //     return script;
    // }
    //
    // /**
    //  * 数据库中查询运费模板
    //  *
    //  * @param templateId
    //  * @return
    //  */
    // private ShipTemplateDO getOneDB(Integer templateId) {
    //
    //     return this.daoSupport.queryForObject(ShipTemplateDO.class, templateId);
    // }

}
