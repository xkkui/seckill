package com.ruyuan.seckill.service;


import com.ruyuan.seckill.domain.vo.CacheGoods;
import com.ruyuan.seckill.domain.vo.GoodsSkuVO;

/**
 * 商品对外的接口
 */
public interface GoodsClient {

    /**
     * 缓存中查询sku信息
     *
     * @param skuId
     * @return
     */
    GoodsSkuVO getSkuFromCache(Integer skuId);

    /**
     * 缓存中查询商品的信息
     *
     * @param goodsId
     * @return
     */
    CacheGoods getFromCache(Integer goodsId);
}
