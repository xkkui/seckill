package com.ruyuan.seckill.service;


import com.ruyuan.seckill.domain.Buyer;
import com.ruyuan.seckill.domain.vo.CartPromotionVo;
import com.ruyuan.seckill.domain.vo.SelectedPromotionVo;

/**
 * 购物车优惠信息处理接口
 * 负责促销的使用、取消、读取。
 * 文档请参考：
 */
public interface CartPromotionManager {


    SelectedPromotionVo getSelectedPromotion();

    SelectedPromotionVo getSelectedPromotion(Buyer buyer);


    /**
     * 使用一个促销活动
     *
     * @param sellerId
     * @param skuId
     * @param promotionVo
     */
    void usePromotion(Integer sellerId, Integer skuId, CartPromotionVo promotionVo);
    //
    //
    // /**
    //  * 使用一个优惠券
    //  *
    //  * @param sellerId
    //  * @param mcId
    //  * @param cartList
    //  */
    // void useCoupon(Integer sellerId, Integer mcId, List<CartVO> cartList);


    /**
     * 删除一个店铺优惠券的使用
     *
     * @param sellerId
     */
    void deleteCoupon(Integer sellerId);

    /**
     * 清除一个所有的优惠券
     */
    void cleanCoupon();

    /**
     * 清除一个所有的优惠券
     */
    void cleanCoupon(Buyer buyer);

    /**
     * 批量删除sku对应的优惠活动
     *
     * @param skuids
     */
    void delete(Integer skuids[]);

    /**
     * 批量删除sku对应的优惠活动
     *
     * @param skuids
     */
    void delete(Integer skuids[], Buyer buyer);

    /**
     * 根据sku检查并清除无效的优惠活动
     *
     * @param skuId
     * @param promotionId
     * @param sellerId
     * @param promotionType
     * @return
     */
    boolean checkPromotionInvalid(Integer skuId, Integer promotionId, Integer sellerId, String promotionType);


    /**
     * 清空当前用户的所有优惠活动
     */
    void clean();

    /**
     * 检测并清除无效活动
     */
    void checkPromotionInvalid();
    /**
     * 检测并清除无效活动
     */
    void checkPromotionInvalid(Buyer buyer);
}
